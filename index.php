<?php
    
    if(isset($_POST['upload']))
    {
        //echo "<pre>";
        //print_r ($_FILES["imageFile"]);
        

        $fileName = $_FILES["imageFile"]["name"];
        $fileSize = $_FILES["imageFile"]["size"]; // bytes
        $fileTemName = $_FILES["imageFile"]["tmp_name"];

        $destinationFolder = "images/";
        $destinationFile =  $destinationFolder . $fileName;
        
        /*
            echo "File Name: $fileName <br>";
            echo "File Size: $fileSize <br>";
            echo "File Name: $fileTemName <br>";
            echo "Destination File: $destinationFile<br>";
        */
        

        //Start To Write your code
        


        move_uploaded_file($fileTemName, $destinationFile);






        // Comment Your github or gitlab link
        // Group Name: PONDITSEIP - PHP with Laravel Framework - B14
        // Post Link: https://www.facebook.com/groups/1336691237255118/?multi_permalinks=1348015516122690&ref=share
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Image Upload Game (PHP)</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://kit.fontawesome.com/e7104424f1.js" crossorigin="anonymous"></script>
</head>
<body>
    <div class="container p-5 pt-3 my-5 bg-light">
        <h2 class="text-center mb-4 fw-bold">Image Upload Game (PHP)</h2>
        <form action="" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col col-sm-12 col-md-10 mb-3">
                    <input class="form-control" type="file" name="imageFile">
                </div>
                <div class="col col-sm-12 col-md-2 mb-3">
                    <input type="submit" class="form-control" value="Upload" name="upload">
                </div>
            </div>
        </form>
    </div>
</body>
</html>